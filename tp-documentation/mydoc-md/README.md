# This is mydoc-md

## Licence
This project is under le MIT Licence

[the readme](readme.md)

## List
- List 1
- List 2

## Table
| **column 1** | **column2** |
|--------------|-------------|
|    foo       |    bar      |
|    foo       |    bar      |

## Code
```hs
main::IO()
main = putStrLn "Hello"
````

>Test

![](https://upload.wikimedia.org/wikipedia/commons/9/9a/Visual_Studio_Code_1.35_icon.svg)