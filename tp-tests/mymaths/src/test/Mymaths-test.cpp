#include <mymaths/mymaths.hpp>

#include <catch2/catch.hpp>

TEST_CASE( "mul2 0..3", "[mul2]" ) {
    REQUIRE( mul2(0) == 0 );
    REQUIRE( mul2(1) == 2 );
    REQUIRE( mul2(2) == 4 );
    REQUIRE( mul2(3) == 6 );
}

TEST_CASE( "muln (3, 4)", "[muln]" ) {
    REQUIRE( muln(3, 4) == 12 );
}

TEST_CASE( "add2 0..3", "[add2]" ) {
    REQUIRE( add2(0) == 2 );
    REQUIRE( add2(1) == 3 );
    REQUIRE( add2(2) == 4 );
    REQUIRE( add2(3) == 5 );
}

TEST_CASE( "addn 3 4", "[addn]" ) {
    REQUIRE( addn(3, 4) == 7 );
}