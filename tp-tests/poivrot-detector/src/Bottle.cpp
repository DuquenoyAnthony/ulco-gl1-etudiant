#include "Bottle.hpp"

#include <sstream>

double alcoholVolume(const Bottle & b) {
    // TODO
    return (b._deg*b._vol)/100;
}

double alcoholVolume(const std::vector<Bottle> & bs) {
    // TODO
    double s = 0;
    for (auto x : bs)
        s += alcoholVolume(x);

    return s;
}

bool isPoivrot(const std::vector<Bottle> & bs) {
    // TODO
    return (alcoholVolume(bs) > 0.0001);
}

std::vector<Bottle> readBottles(std::istream & is) {
    // TODO
    std::vector<Bottle> v;
    Bottle b;

    while (is) 
    {
        std::string contents;
        std::getline(is, contents);

        std::stringstream str(contents);
        std::string tmp;
        char *p;
        std::getline(str, tmp, ';');
        b._name = tmp;
        std::getline(str, tmp, ';');
        b._vol = std::strtod(tmp.c_str(), &p);
        std::getline(str, tmp, ';');
        b._deg = std::strtod(tmp.c_str(), &p);

        v.push_back(b);
    }
    return v;
}

